package com.example.latihan1.Welcome_Activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.latihan1.Home_Activity.HomeActivity
import com.example.latihan1.Login_Activity.LoginActivity
import com.example.latihan1.R
import com.example.latihan1.Register_Activity.RegisterActivity
import com.example.latihan1.SharedPref
import kotlinx.android.synthetic.main.welcome_activity.*

class Welcome_Activity:AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcome_activity)
        button1()
        button2()
    }
    fun button1(){
        btn_login.setOnClickListener {
            val perpindahan = Intent(this@Welcome_Activity, LoginActivity::class.java)
            startActivity(perpindahan)
        }
    }
    fun button2(){
        btn_regis.setOnClickListener {
            val dadu = Intent(this@Welcome_Activity, RegisterActivity::class.java)
            startActivity(dadu)
        }
    }


    override fun onStart() {
        super.onStart()

        val sharedPreference: SharedPref = SharedPref(this)
        val gogo1 = sharedPreference.getValueString3("aktifemail")
        val gogo2 = sharedPreference.getValueString4("aktifpass")


        if (gogo1!=null && gogo2!=null) {
            val dadu = Intent(this@Welcome_Activity, HomeActivity::class.java)
            startActivity(dadu)
            finish()
        }

    }
}