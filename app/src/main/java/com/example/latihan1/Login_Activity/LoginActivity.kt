package com.example.latihan1.Login_Activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.latihan1.Home_Activity.HomeActivity
import com.example.latihan1.R
import com.example.latihan1.SharedPref
import com.example.latihan1.Welcome_Activity.Welcome_Activity
import kotlinx.android.synthetic.main.login_activity.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        imgbutton()
        textwajib()
    }

    private fun imgbutton() {
        btn_backfromlogin.setOnClickListener {
            val mybutton = Intent(this@LoginActivity, Welcome_Activity::class.java)
            startActivity(mybutton)
        }
    }

    private fun textwajib() {
        btn_login_onlogin.setOnClickListener {
            val sharedPreference: SharedPref = SharedPref(this)

            val emailRegex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
            var validasi = true
            val emailEdit = et_login_email.editableText.toString()
            val passEdit = et_login_pass.editableText.toString()
            val emailSharedpref = sharedPreference.getValueString("email")
            val passSharedPref = sharedPreference.getValueString1("pass")
            val emailString = et_login_email.text.toString()
            val passString = et_login_pass.text.toString()

            if (emailString.isEmpty() || emailString.isBlank()) {
                et_login_email.error = "email harus terisi"
                et_login_pass.requestFocus()
                validasi = false
            } else if (!emailString.trim().matches(emailRegex.toRegex())) {
                et_login_email.error = "email tidak valid"
                et_login_pass.requestFocus()
                validasi = false
            } else if (passString.isEmpty() || passString.isBlank()) {
                et_login_pass.error = "Password harus terisi"
                et_login_email.requestFocus()
                validasi = false
            } else if (!emailEdit.equals(emailSharedpref)) {
                Toast.makeText(this, "email atau password kamu salah", Toast.LENGTH_LONG).show()
                validasi = false
            } else if (!passEdit.equals(passSharedPref)) {
                Toast.makeText(this, "email atau password kamu salah", Toast.LENGTH_LONG).show()
                validasi = false
            } else if (emailEdit.equals(emailSharedpref) && passEdit.equals(passSharedPref)) {
                val gogoi = Intent(this@LoginActivity, HomeActivity::class.java)
                startActivity(gogoi)
                validasi = true
            }

            if (validasi == true) {
                sharedPreference.aktifemail("aktifemail", emailEdit)
                sharedPreference.aktifpass("aktifpass", passEdit)
            }

        }
    }
}
/*fun forshare() {
    val sharedPreferences = getSharedPreferences("SP_INFO", Context.MODE_PRIVATE)

    val getemail = sharedPreferences.getString("EMAIL", "")
    val getpass = sharedPreferences.getString("PASS", "")



}*/
