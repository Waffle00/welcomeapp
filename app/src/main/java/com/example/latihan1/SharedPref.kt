package com.example.latihan1

import android.content.Context
import android.content.SharedPreferences

class SharedPref(context: Context) {

    private val sharedPrefFile = "kotlinshared"

    val sharedPreferences: SharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)

    fun name(name_key: String, name: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(name_key, name)
        editor.apply()
    }

    fun save(email_key: String, email: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(email_key, email)
        editor.apply()
    }
    fun instansi(instansi_key: String, instansi: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(instansi_key, instansi)
        editor.apply()
    }
    fun pass(pass_key: String, pass: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(pass_key, pass)
        editor.apply()
    }
    fun aktifemail(aktife_key : String, aktifemail: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(aktife_key, aktifemail)
        editor.apply()
    }
    fun aktifpass(aktifp_key : String, aktifpass: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(aktifp_key, aktifpass)
        editor.apply()
    }

    fun getValueString(email_key: String): String? {
        return sharedPreferences.getString(email_key, null)
    }
    fun getValueString1(pass_key: String): String? {
        return sharedPreferences.getString(pass_key, null)
    }
    fun getValueString2(name_key: String): String? {
        return sharedPreferences.getString(name_key, null)
    }
    fun getValueString3(aktife_key: String): String? {
        return  sharedPreferences.getString(aktife_key, null)
    }
    fun getValueString4(aktifp_key: String): String? {
        return  sharedPreferences.getString(aktifp_key, null)
    }

    fun removeValue(aktife_key: String) {

        val editor: SharedPreferences.Editor = sharedPreferences.edit()

        editor.remove(aktife_key)
        editor.apply()
    }

    fun removeValue1(aktifp_key: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()

        editor.remove(aktifp_key)
        editor.apply()
    }



}