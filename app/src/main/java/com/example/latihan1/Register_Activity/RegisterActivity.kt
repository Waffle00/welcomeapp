package com.example.latihan1.Register_Activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.latihan1.Login_Activity.LoginActivity
import com.example.latihan1.R
import com.example.latihan1.SharedPref
import com.example.latihan1.Welcome_Activity.Welcome_Activity
import kotlinx.android.synthetic.main.register_activity.*

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_activity)
        imgbutton1()
        regbutton()

    }

    private fun imgbutton1() {
        btn_backfromregis.setOnClickListener {
            val mybutton = Intent(this@RegisterActivity, Welcome_Activity::class.java)
            startActivity(mybutton)
        }
    }

    private fun regbutton() {
        btn_regis_onregis.setOnClickListener {
            val sharedPreference: SharedPref = SharedPref(this)

            val regisUsername: String = et_regis_username.text.toString()
            val regisEmail: String = et_regis_email.text.toString()
            val regisUniv: String = et_regis_univ.text.toString()
            val regisPass: String = et_regis_pass.text.toString()
            val regisConfirmpass: String = et_regis_confirmpass.text.toString()
            val emailRegex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
            val name = et_regis_username.editableText.toString()
            val email = et_regis_email.editableText.toString()
            val pass = et_regis_pass.editableText.toString()
            val univ = et_regis_univ.editableText.toString()
            val emailPref = sharedPreference.getValueString("email")
            val passPref = sharedPreference.getValueString2("name")
            var validasi = true

            if (regisUsername.isEmpty() || regisUsername.isBlank()) {
                et_regis_username.error = "nama harus terisi"
                et_regis_username.requestFocus()
                validasi = false
            } else if (regisEmail.isEmpty() || regisEmail.isBlank()) {
                et_regis_email.error = "email harus terisi"
                et_regis_email.requestFocus()
                validasi = false
            } else if (!regisEmail.trim().matches(emailRegex.toRegex())) {
                et_regis_email.error = "email tidak valid"
                et_regis_email.requestFocus()
                validasi = false
            } else if (regisUniv.isEmpty() || regisUsername.isBlank()) {
                et_regis_univ.error = "instansi harus terisi"
                et_regis_univ.requestFocus()
                validasi = false
            } else if (regisPass.length < 6) {
                et_regis_pass.error = "Password minimal 6 karakter"
                et_regis_pass.requestFocus()
            } else if (regisPass.length > 10) {
                et_regis_pass.error = "Password maksimal 10 karakter"
                et_regis_pass.requestFocus()
                validasi = false
            }else if (regisConfirmpass != regisPass) {
                et_regis_confirmpass.error = "password tidak sama"
                et_regis_confirmpass.requestFocus()
                validasi = false
            } else if (name.equals(emailPref) && email.equals(passPref)) {
                Toast.makeText(this, "akun sudah ada", Toast.LENGTH_SHORT).show()
                validasi = false
            } else if (!name.equals(emailPref) && !email.equals(passPref)) {
                val gogoi1 = Intent(this@RegisterActivity, LoginActivity::class.java)
                startActivity(gogoi1)
                finish()
                validasi = true
            }

            if (validasi == true) {
                sharedPreference.name("name", name)
                sharedPreference.save("email", email)
                sharedPreference.pass("pass", pass)
                sharedPreference.instansi("instansi", univ)
            }



        }

    }
}