package com.example.latihan1.Home_Activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.latihan1.R
import com.example.latihan1.SharedPref
import com.example.latihan1.Welcome_Activity.Welcome_Activity
import kotlinx.android.synthetic.main.home_activity.*

class HomeActivity : AppCompatActivity() {
    private lateinit var rvAndro: RecyclerView
    private var list: ArrayList<HomeParameter> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
        rvAndro = findViewById(R.id.rv_home)
        rvAndro.setHasFixedSize(true)

        list.addAll(HomeData.listData)
        showRecyclerList()
        buttonLogout()

    }

    private fun showRecyclerList() {
        rvAndro.layoutManager = LinearLayoutManager(this)
        val listHeroAdapter = HomeAdapter(list)
        rvAndro.adapter = listHeroAdapter

    }

    private fun buttonLogout() {
        val sharedPreference: SharedPref = SharedPref(this)
        btn_logout.setOnClickListener {
            sharedPreference.removeValue("aktifemail")
            sharedPreference.removeValue1("aktifpass")
            val gogoi1 = Intent(this@HomeActivity, Welcome_Activity::class.java)
            startActivity(gogoi1)
            finish()
        }
    }
}