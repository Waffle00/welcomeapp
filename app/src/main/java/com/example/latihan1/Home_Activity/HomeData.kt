package com.example.latihan1.Home_Activity

object HomeData {
    private val androidName = arrayOf(
        "pie", "Oreo", "Oreo", "Nougat", "Nougat",
        "Marsmallow", "Lolipop"
    )

    private val androidVersion = arrayOf(
        "Android 9.0", "Android 8.1", "Version 8.0",
        "Version 7.1", "Version 7.0", "Version 6.0", "version 5.1"
    )

    private val APIVersion = arrayOf(
        "API Level 28", "API Level 27", "API Level 26", "API Level 25",
        "API Level 24", "API Level 23", "API Level 22"
    )

    val listData: ArrayList<HomeParameter>
        get() {
            val list = arrayListOf<HomeParameter>()
            for (position in androidName.indices) {
                val androPoss = HomeParameter()
                androPoss.androName = androidName[position]
                androPoss.androversion = androidVersion[position]
                androPoss.APIVersion = APIVersion[position]
                list.add(androPoss)
            }
            return list

        }
}