package com.example.latihan1.Home_Activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.latihan1.R

class HomeAdapter(private val listAndroid: ArrayList<HomeParameter>) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recycleview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listAndroid.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val andro = listAndroid[position]
        holder.tvandroidname.text = andro.androName
        holder.tvandroidversion.text = andro.androversion
        holder.tvapiversion.text = andro.APIVersion

    }

    inner class ViewHolder(inihome: View) : RecyclerView.ViewHolder(inihome) {
        var tvandroidname: TextView = itemView.findViewById(R.id.tv_androidname)
        var tvandroidversion: TextView = itemView.findViewById(R.id.tv_androidversion)
        var tvapiversion: TextView = itemView.findViewById(R.id.tv_apiversion)


    }
}



